/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */

// eslint-disable-next-line import/no-import-module-exports
import dotenv from 'dotenv';

dotenv.config({ path: '.env' });

const path = require('path');

module.exports = {
  siteMetadata: {
    title: 'Cafuné cafe',
    titleTemplate: '%s - Cafuné cafe',
    description:
      'Based in the very heart of Preston, Cafuné is the first South American coffee shop in the UK. We serve the best coffee and food that the continent has to offer in a vibrant, welcoming atmosphere. No matter the weather, it&apos;s always sunny at Cafuné!',
    url: 'https://www.cafune.co.uk',
    image: '/images/open-graph.jpg', // Path to your image you placed in the 'static' folder
    twitterUsername: '@cafunecafeuk',
  },
  plugins: [
    'gatsby-plugin-styled-components',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: path.join(__dirname, `src`, `assets`, `images`),
      },
    },
    `gatsby-plugin-image`,
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [`Open Sans:400,600`],
        display: 'swap',
      },
    },
    {
      resolve: 'gatsby-source-sanity',
      options: {
        projectId: '9mwq4c79',
        dataset: 'production',
        token: process.env.SANITY_TOKEN,
        watchMode: true,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Cafuné cafe`,
        short_name: `Cafuné cafe`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#ffffff`,
        display: `standalone`,
        icon: `src/assets/images/feather.svg`,
      },
    },
    {
      resolve: `gatsby-plugin-facebook-pixel`,
      options: {
        pixelId: '506729553718490',
      },
    },
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        trackingIds: ['UA-120600958-1'],
      },
    },
  ],
};
