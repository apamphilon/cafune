import React from 'react';
import { GatsbyImage } from 'gatsby-plugin-image';
import styled from 'styled-components';

const StyledCard = styled.div`
  background-color: white;
  position: relative;
  overflow: hidden;
`;

const Inner = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
  height: 100%;
  padding: 3rem;

  @media screen and (min-width: 1400px) {
    padding: ${({ padding }) => padding};
  }
`;

export default function Card({
  children,
  fluid,
  padding = '3rem',
  radius = '1rem',
}) {
  return (
    <StyledCard radius={radius}>
      {fluid && <GatsbyImage image={fluid} />}
      <Inner padding={padding}>{children}</Inner>
    </StyledCard>
  );
}
