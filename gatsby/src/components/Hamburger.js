import React from 'react';
import styled from 'styled-components';
import { brand } from '../styles/colors';

const StyledBurger = styled.button`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  width: 2.5rem;
  height: 2rem;
  background: transparent;
  border: none;
  cursor: pointer;
  padding: 0;
  z-index: 10;

  &:focus {
    outline: none;
  }

  div {
    width: 2.5rem;
    height: 0.25rem;
    background: ${brand};
    border-radius: 10px;
    transition: all 0.3s linear;
    position: relative;
    transform-origin: 3px;

    :first-child {
      transform: ${({ isActive }) =>
        isActive ? 'rotate(45deg)' : 'rotate(0)'};
    }

    :nth-child(2) {
      opacity: ${({ isActive }) => (isActive ? '0' : '1')};
      transform: ${({ isActive }) =>
        isActive ? 'translateX(20px)' : 'translateX(0)'};
    }

    :nth-child(3) {
      transform: ${({ isActive }) =>
        isActive ? 'rotate(-45deg)' : 'rotate(0)'};
    }
  }
`;

export default function Hamburger({ isActive, onClick, className }) {
  return (
    <StyledBurger
      type="button"
      onClick={onClick}
      isActive={isActive}
      className={className}
    >
      <div />
      <div />
      <div />
    </StyledBurger>
  );
}
