import React from 'react';
import styled from 'styled-components';

const StyledQuoteSection = styled.div`
  padding: 10rem 2rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: #2ba15e;
`;

const Container = styled.div`
  max-width: 43rem;
`;

const Title = styled.span`
  display: block;
  font-size: 4.8rem;
  color: white;
  margin-bottom: 2rem;
`;

const Definition = styled.span`
  color: rgba(255, 255, 255, 0.8);
  font-size: 2.2rem;
`;

export default function QuoteSection() {
  return (
    <StyledQuoteSection>
      <Container>
        <Title>cafuné</Title>
        <Definition>
          (n.) running your fingers through the hair of someone you love
        </Definition>
      </Container>
    </StyledQuoteSection>
  );
}
