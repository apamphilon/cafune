import { useStaticQuery, graphql } from 'gatsby';

export default function useGalleryQuery() {
  const data = useStaticQuery(graphql`
    query {
      allSanityFileAsset {
        edges {
          node {
            id
            title
            url
          }
        }
      }
    }
  `);

  return data.allSanityFileAsset.edges[3].node.url;
}
