import { TiContacts as icon } from 'react-icons/ti';

export default {
  name: 'contact',
  title: 'Contact',
  type: 'document',
  icon,
  fields: [
    {
      name: 'address',
      title: 'Address',
      type: 'text',
    },
    {
      name: 'directions',
      title: 'Directions link',
      type: 'url',
    },
    {
      name: 'hours',
      title: 'Opening hours',
      type: 'text',
    },
    {
      name: 'email',
      title: 'Email',
      type: 'string',
    },
  ],
  preview: {
    prepare: () => ({
      title: 'Contact',
    }),
  },
};
