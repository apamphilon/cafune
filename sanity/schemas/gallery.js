export default {
  name: 'gallery',
  title: 'Gallery',
  type: 'document',
  fields: [
    {
      name: 'images',
      type: 'array', // supports drag'n'drop of multiple files
      options: {
        layout: 'grid',
      },
      of: [
        {
          type: 'image',
        },
      ],
    },
  ],
  preview: {
    prepare: () => ({
      title: 'Gallery',
    }),
  },
};
